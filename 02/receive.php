
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
  </head>
  <body>
    <pre>
    <?php
var_dump($_GET);
?>
</pre>
    <table border="1" cellpadding="6" cellspacing="0">
      <caption>以下の内容で送信しました。</caption>
      <tr>
        <td>メールアドレス</td>
        <td>
          <?php echo $_GET['mailaddres']; ?>
        </td>
      </tr>
      <tr>
        <td>名前</td>
        <td>
          <?php echo $_GET['namae'];?>
        </td>
      </tr>
      <tr>
        <td>性別</td>
        <td>
          <?php echo $_GET['seibetu'];?>
        </td>
      </tr>
      <tr>
        <td>趣味</td>
      <td>
        <?php if (isset($_GET['hobby'])) {
    $hobby = implode(", ", $_GET['hobby']);
    echo '趣味は：' . $hobby . '<br>';
} else {
    echo 'チェックされていません。<br>';
}
?>
      </td>
      </tr>
      <tr>
        <td>誕生日</td>
        <td>
        <?php echo $_GET['month'];?>月
        <?php echo $_GET['day'];?>日
        </td>
      </tr>
      <tr>
        <td>その他・感想</td>
        <td>
          <?php echo $_GET['sonota'];?>
        </td>
      </tr>
  </body>
</html>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習画面</title>
  </head>
  <body>
    <?php
    $weapon = array('大剣','太刀','片手剣','双剣','ランス','ガンランス','ハンマー','狩猟笛');
    echo $weapon[3]; //双剣
    echo $weapon[0]; //大剣
    echo $weapon[10]; //なし
    $weapon[7] = 'スラッシュアックス'; //狩猟笛⇒スラッシュアックス
    $weapon[8] = 'ボウガン'; //[8]にボウガンを追加
    var_dump($weapon);
    ?>
    <pre>
      <?php var_dump($weapon); ?>
    </pre>
  </body>
</html>

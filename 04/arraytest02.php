<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習画面</title>
  </head>
  <body>
    <?php
    $weapon = array('大剣','太刀','片手剣','双剣','ランス','ガンランス','ハンマー','狩猟笛');
    var_dump($weapon);
    for ($i=0; $i < count($weapon) ; $i++) {
      echo $weapon[$i] . "<br/>";
    }
    ?>
    <hr>
    <?php
    $weapon = array('大剣','太刀','片手剣','双剣','ランス','ガンランス','ハンマー','狩猟笛');
    foreach ($weapon as $each) {
      echo $each . "<br/>";
    }
    echo "<hr>";
    $needle = "太刀";
    if (in_array($needle,$weapon)) {
      echo $needle . "がweaponの要素の値に存在しています。";
    } else {
      echo $needle . "がweaponの要素の値に存在していません。";
    }
    ?>
  </body>
</html>

<!DOCTYPE html>
<?php
if (is_uploaded_file($_FILES["jsonfile"]["tmp_name"])) {
    $file_tmp_name = $_FILES["jsonfile"]["tmp_name"];
    $file_name = $_FILES["jsonfile"]["name"];

    if (pathinfo($file_name, PATHINFO_EXTENSION) != 'json') { //jsonファイルか確認
        echo 'jsonファイルのみ対応しています。<br>';
        include('./include/error.php');
    } else {
        //tmp(一時ファイル)ディレクトリ内のjsonファイルをdateディレクトリに移動
        if (move_uploaded_file($file_tmp_name, "/tmp/" . $file_name)) {
            chmod("/tmp/" . $file_name, 0644);
            $msg = $file_name . "アップロードしました。";
            $file = '/tmp/' .$file_name;

            //jsonファイルを文字列にして読み込む
            $temp_json_str = file_get_contents($file);

            //文字列として読み込んだjsonを連想配列にする
            $temp_json = json_decode($temp_json_str, true);
            unlink('/tmp/' .$file_name);

            include("./include/functions.php");
            $pdo = initDB();

            echo "<pre>";
            var_dump($temp_json);
            echo "</pre>";

            $values_loop = "";
            //foreachでループしてSQL文のvalue以降を作成
            foreach ($temp_json as $each){
                $values = "('" . $each['name'] . "'," . $each['pref'] . "," . $each['seibetu'] . "," . $each['age'] . "," . $each['section_ID'] . "," . $each['grade_ID'] . "),";
                echo $values . "<hr/>";
                $values_loop .= $values;
                //作成したSQL文を$values_loopに追加していく
            }
            //最後にカンマが余分に残るので1文字分を削除。
            $values_loop = substr($values_loop, 0, -1);

            $query_str = "INSERT INTO
                          member(name, pref, seibetu, age, section_ID, grade_ID)
                          VALUES " . $values_loop . "";
                          //SQL文に$values_loopを追加して実行

            echo "$query_str";

            try {
                $spl = $pdo->prepare($query_str);
                $spl->execute();

                //トップ画面にリダイレクトする
                header('location:index.php');
                exit;

            } catch (PDOException $e) {
                print $e->getMessage();
            }
        } else {
            echo "ファイルをアップロードできません。<br>";
            include('./include/error.php');
        }
    }
} else {
    echo "ファイルが選択されていません。<br>";
    include('./include/error.php');

}
 ?>

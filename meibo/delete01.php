<!DOCTYPE html>
<?php
include('./include/functions.php');
include('./include/statics.php');
$pdo = Initdb(); //DBのログイン文を呼び出し

//member_idの値チェック
if (isset($_GET['member_ID'])&& $_GET['member_ID'] != "") {
    $query_str = "DELETE FROM `member` WHERE member_ID = " . $_GET['member_ID'] . ";";
    //SQL文で削除を行う

    $sql = $pdo->prepare($query_str);
    $sql ->execute();

    header('location:index.php'); //トップ画面にリダイレクトする
    exit;
} else {
    echo "社員IDが見つかりません。<br/>"; //ヘッダー文を呼び出し
    include('./include/error.php'); //エラー文を呼び出し
}
 ?>

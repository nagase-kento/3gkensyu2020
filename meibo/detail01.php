<!DOCTYPE html>
<?php
include('./include/functions.php');
include('./include/statics.php');
$pdo = Initdb(); //DBのログイン文を呼び出し

//member_idの値チェック
if (isset($_GET['member_ID']) && $_GET['member_ID'] != "") {
    $query_str = "SELECT m.member_ID,m.name,m.pref,m.seibetu,m.age,sm.section_name,gm.grade_name
                  FROM member AS m
                  LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                  LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                  WHERE m.member_ID = " . $_GET['member_ID'] . "";
                  //DBのデータをmember_IDを指定して呼び出し

    $sql = $pdo->prepare($query_str);
    $sql ->execute();
    $result = $sql ->fetchAll();

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript"> //javascriptタブ
    <!--
    function deleteCheck() {
        if (window.confirm('削除します。よろしいですか。')) { //削除押下時に確認ダイアログを表示
            document.sakujo.submit(); //「はい」を押下するとsakujoフォームがsubmitされる
        }
    }
    -->
    </script>
    <title>社員名簿</title>
  </head>
  <body>
    <?php
    include('./include/header.php'); //ヘッダー文を呼び出し
     ?>
    <?php
    //呼び出したデータの数をcountで数えている１以外取れた場合、エラー文を表示
    if (count($result) != 1) {
        echo "存在しない社員IDです<br/>";
        include('./include/error.php');
    }else { //呼び出したデータを以下で表示
     ?>
     <!-- ここから下テーブル 。DBで四b出したデータをechoで呼び出し-->
    <div class="search_resurt">
    <table class='table table-bordered'>
      <tr>
        <th class="table-active" width="103px"> 社員ID </th>
        <td><?php echo $result[0]['member_ID']; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 名前 </th>
        <td><?php echo $result[0]['name']; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 出身地 </th>
        <td><?php echo $pref_array[$result[0]['pref']]; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 性別 </th>
        <td><?php echo $gender_array[$result[0]['seibetu']]; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 年齢 </th>
        <td><?php echo $result[0]['age']; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 所属部署 </th>
        <td><?php echo $result[0]['section_name']; ?></td>
      </tr>
      <tr>
        <th class="table-active"> 役職 </th>
        <td><?php echo $result[0]['grade_name']; ?></td>
      </tr>
    </table>
    <!-- ここまでテーブル -->
    <table align=right>
      <tr>
        <form action='entry_update01.php' method='GET' name='hensyu'>
          <!-- entry_update01.phpに遷移する際に、hiddenでmember_idを渡している -->
          <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
          <td><input type="submit" value="編集"></td>
        </form>
        <form action='delete01.php' method='GET' name='sakujo'>
          <!-- delete01.phpに遷移する際に、hiddenでmember_idを渡している -->
          <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
          <td><input type="button" value="削除" onClick="deleteCheck();"></td>
        </form>
      </tr>
    </table>
    <?php
    }
    // echo "<pre>";
    // var_dump($result);
    // echo "</pre>";
     ?>
    <?php
}else { //7行目のeles。member_IDが不正の時にエラー文を表示
    echo "社員IDが見つかりません。<br/>";
    include('./include/error.php');
}
    ?>
    <?php
    include('./include/footer.php');  //フッター文を呼び出し
     ?>
    </div>
  </body>
</html>

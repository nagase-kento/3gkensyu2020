function submitCheck(){
  var temp_name = document.mainform.namae.value;
  temp_name = temp_name.replace(/\s+/g,""); //ここでスペースを排除
  document.mainform.namae.value = temp_name;
  if (temp_name == "") { //名前に値が入っているかを確認
    alert("名前は必須項目です"); //入っていない場合、エラーダイアログを表示
    return fales;
  }

  var temp_age = document.mainform.age.value;
  if (temp_age == "" || Number.isInteger(temp_age) || temp_age < 1 || temp_age > 100) { //年齢に値が入っているか、数値を以外の値が入っていないか(Number.isInteger())、1以上100以下かを確認
    alert("年齢は必須項目です/1~99の数値を入れてください"); //if文の条件のどれか1つで当てはまるとエラー
    return false;
  }

  if (document.mainform.todouhuken.value == "") { //都道府県に値が入っているかの確認
    alert("都道府県は必須です");
    return false;
  }

  if (window.confirm('登録します。よろしいですか。')) {
    document.mainform.submit();
 }
}

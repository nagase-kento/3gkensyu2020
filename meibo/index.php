<!DOCTYPE html>
<?php
include('./include/functions.php'); //[./include/functions.php]をインクルード
include('./include/statics.php'); //[./include/statics.php]をインクルード
$pdo = Initdb(); //DBのログイン文を呼び出し

$query_str = "SELECT m.member_ID,m.name,sm.section_name,gm.grade_name,m.seibetu,m.pref,m.age
              FROM member AS m
              LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
              LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
              WHERE 1=1 "; //SQL文でDBのデータを呼び出す。呼び出すデータは社員IDと名前と部署と役職

               //トップ画面の検索欄に名前が入力された場合、上記のSQL文に以下のSQL文が追加される。
              if (isset($_GET['namae']) && $_GET['namae'] != ""){
                $query_str .= "AND m.name LIKE '%" . $_GET['namae'] . "%'";
              }

              if (isset($_GET['seibetu']) && $_GET['seibetu'] != ""){
                $query_str .= "AND m.seibetu = '" . $_GET['seibetu'] . "'";
              }

              if (isset($_GET['busyo']) && $_GET['busyo'] != ""){
                $query_str .= "AND m.section_ID = '" . $_GET['busyo'] . "'";
              }

              if (isset($_GET['yakusyoku']) && $_GET['yakusyoku'] != ""){
                $query_str .= "AND m.grade_ID = '" . $_GET['yakusyoku'] . "'";
              }

$sql = $pdo->prepare($query_str);
$sql ->execute();
$result = $sql ->fetchAll(); //呼び出したDB配列にする

          //ここからcsvダウンロードの処理を記載
if (array_key_exists('csv', $_GET) && $_GET['csv'] == "1"){
    try {
      //出力するファイル名を指定
      $csvFileName = '/tmp/' . time() . rand() . '.csv'; //出力するファイル名を指定
      //ファイルの書き込み　fopen=ファイル書き込み 'w'=書き込み専用（ファイルがなければ自動生成）
      $res = fopen($csvFileName, 'w');
      if ($res === FALSE) {
          throw new Exception('ファイルの書き込みに失敗しました。');
      }
      $dataList = array(
              array('社員番号', '名前', '年齢', '性別', '出身', '役職', '部署')
      );

      // ここでDB検索結果を$datalistの配列に追加していく
      foreach($result as $row){
          $temp_array = array($row['member_ID'],$row['name'],$row['age'],$gender_array[$row['seibetu']],$pref_array[$row['pref']],$row['grade_name'],$row['section_name']);
          array_push($dataList, $temp_array); //array_push=配列の最後に要素を追加する
      }
      // ループしながら出力
      foreach($dataList as $dataInfo) {
          // 文字コード変換。エクセルで開けるようにする
          mb_convert_variables('SJIS', 'UTF-8', $dataInfo);
          // ファイルに書き出しをする
          fputcsv($res, $dataInfo);
      }
        // ハンドル閉じる
      fclose($res);

      // ダウンロード修正版。参考：https://teratail.com/questions/40474
      header('Content-Description: File Transfer');
      header("Content-Disposition: attachment; filename=workerlist" . date(YmdHis) . ".csv");
      header('Content-Type: application/force-download;');

      ob_clean();
      flush();

      readfile($csvFileName);
      exit;

    } catch(Exception $e) {
      // 例外処理をここに書きます
      echo $e->getMessage();
      }
}
          //ここまでcsvダウンロード処理
          //ここからisonダウンロードの処理を記載
if (array_key_exists('json', $_GET) && $_GET['json'] == "1") { //getでjson=１になってたら実行
    try {
        //出力するファイル名を指定
        $csvFileName = '/tmp/' . time() . rand() .'.json';
        //ファイルの書き込み　fopen=ファイル書き込み 'w'=書き込み専用（ファイルがなければ自動生成）
        $res = fopen($csvFileName, 'w');
        if ($res === FALSE) {
            throw new Exception('ファイルの書き込みに失敗しました。');
        }
        $dataList = array();
        //Jsonに、DBの検索結果を連想配列で作成していく
        foreach ($result as $row) {
            $temp_array = array(
              '社員番号'=>$row['member_ID'],
              '名前'=>$row['name'],
              '年齢'=>$row['age'],
              '性別'=>$gender_array[$row['seibetu']],
              '出身地'=>$pref_array[$row['pref']],
              '役職'=>$row['grade_name'],
              '所属部署'=>$row['section_name']);
            array_push($dataList, $temp_array); //array_push=配列の最後に要素を追加する
        }
        //配列をJsonに変換して書き込む
        //fwrite(書き込むファイルを指定 , 書き込む文字列)
        fwrite($res, json_encode($dataList, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE )); //文字化け防止：JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE
        echo $res;
        fclose($res); //ファイルを閉じる

        header('Content-Description: File Transfer');
        header("Content-Disposition: attachment; filename=workerlist" . date(YmdHis) . ".json");
        header('Content-Type: application/force-download;');
        ob_clean();
        flush();
        readfile($csvFileName);
        exit;

    } catch(Exception $e) {
    echo $e->getMessage();
      }
}
          //ここまでjsonダウンロード処理
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script "text/javascript"> //javascript、このページでは検索値のリセットと検索欄の名前にスペースがあった場合、排除する処理を記載
    <!--
    function delspace(){
      var temp = document.mainform.namae.value;
      document.mainform.namae.value = temp.replace(/\s+/g,"");
      //文字列.replace()で指定の文字を別の文字に置き換える。ここではスラッシュをnullに置換。
      //javascriptではスラッシュ/で囲うことで正規表現を使用することができる。
      // /.../gで文字列内のすべての\s+に対し置き換えができる。
      // \s＝スペース \s+＝複数桁のスペース、+を付けることで複数桁に対応することができる
    }

    function clearReset(){
      document.mainform.namae.value = ""; //検索のリセットを初期値に戻す。ここではnullにしている。
      document.mainform.seibetu.value = "";
      document.mainform.busyo.value = "";
      document.mainform.yakusyoku.value = "";
    }

    -->
    </script>
    <title>社員名簿</title>
  </head>
  <body>
    <?php
    include('./include/header.php');  //ヘッダー文を呼び出し
     ?>
    <form name="mainform" action="index.php" method="GET">
      <!-- ここから下が検索欄。optionタグ内のif文は検索後の値を残しておくため -->
      <table align="center" class="search_form">
        <tr>
          <th>名前：</th>
          <td colspan='3'><input type='text' name='namae' value='<?php if(isset($_GET['namae'])){ echo $_GET['namae']; } ?>'></td>
        </tr>
        <tr>
          <th>性別：</th>
          <td><select class="" name="seibetu">
            <option value="">すべて</option>
            <option value="1" <?php if(isset($_GET['seibetu']) && $_GET['seibetu'] == "1"){ echo "selected"; } ?>>男</option>
            <option value="2" <?php if(isset($_GET['seibetu']) && $_GET['seibetu'] == "2"){ echo "selected"; } ?>>女</option>
          </td></select>
          <th>部署：</th>
          <td><select name="busyo">
            <option value="" >すべて</option>
            <option value="1" <?php if(isset($_GET['busyo']) && $_GET['busyo'] == "1"){ echo "selected"; } ?>>第一事業部</option>
            <option value="2" <?php if(isset($_GET['busyo']) && $_GET['busyo'] == "2"){ echo "selected"; } ?>>第二事業部</option>
            <option value="3" <?php if(isset($_GET['busyo']) && $_GET['busyo'] == "3"){ echo "selected"; } ?>>営業</option>
            <option value="4" <?php if(isset($_GET['busyo']) && $_GET['busyo'] == "4"){ echo "selected"; } ?>>総務</option>
            <option value="5" <?php if(isset($_GET['busyo']) && $_GET['busyo'] == "5"){ echo "selected"; } ?>>人事</option>
          </td></select>
          <th>役職：</th>
          <td><select name="yakusyoku">
            <option value="">すべて</option>
            <option value="1" <?php if(isset($_GET['yakusyoku']) && $_GET['yakusyoku'] == "1"){ echo "selected"; } ?>>事業部長</option>
            <option value="2" <?php if(isset($_GET['yakusyoku']) && $_GET['yakusyoku'] == "2"){ echo "selected"; } ?>>部長</option>
            <option value="3" <?php if(isset($_GET['yakusyoku']) && $_GET['yakusyoku'] == "3"){ echo "selected"; } ?>>チームリーダー</option>
            <option value="4" <?php if(isset($_GET['yakusyoku']) && $_GET['yakusyoku'] == "4"){ echo "selected"; } ?>>リーダ－</option>
            <option value="5" <?php if(isset($_GET['yakusyoku']) && $_GET['yakusyoku'] == "5"){ echo "selected"; } ?>>メンバー</option>
          </td></select>
        </tr>
      </table>
      <table align="center">
        <tr>
          <td><input type="submit" name="kensaku" value="検索" onclick="delspace();"></td>
          <td><input type="button" name="torikeshi" value="取り消し" onclick="clearReset();"></td>
        </tr>
      </table>
      <hr/>
    </form>
    <!-- ここまで検索欄 -->
    <!-- ここから下が社員一覧 -->
    <div class="search_resurt">
    <table>
      <?php
	      echo "<tr><td>検索結果：". count($result) . "</td></tr>";
        ?>
    </table>
    <table class="table table-bordered">
      <tr class="table-active">
        <th>社員ID</th>
        <th>名前</th>
        <th>部署</th>
        <th>役職</th>
      </tr>
       </thead>
      <!-- 社員IDの中身は呼び出したDBの配列をforeach（配列のデータ数ループ）でループして表示。 -->
    <?php
    if (count($result) == 0) { //呼び出したDBのデータが0件だった場合、「検索結果なし」と表示
        echo "<tr><th colspan='4'style='text-align: center;'> 検索結果なし </th></tr>";
    }
    foreach ($result as $each) {
        echo "<tr><td>" . $each['member_ID'] . "</td>" .
        "<td><a href='./detail01.php?member_ID=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>" .
        "<td>" . $each['section_name'] . "</td>" .
        "<td>" . $each['grade_name'] . "</td></tr>" ;
    }
    ?>
    </table>
    </div>
    <hr/>
    <form method="get" action="" align="right">
      <input type='hidden' name='csv' value='1'>
      <input type="submit" value="csvエクスポート">
    </form>
    <br/>
    <form method="post" action="importcsv.php" enctype="multipart/form-data" align="right">
      <input type="file" name="csvfile">
      <input type="submit" value="csvインポート">
    </form>
    <hr/>
    <form method="get" action="" align="right">
      <input type='hidden' name='json' value='1'>
      <input type="submit" value="jsonエクスポート">
    </form>
    <br/>
    <form method="post" action="importjson.php" enctype="multipart/form-data" align="right">
      <input type="file" name="jsonfile">
      <input type="submit" value="jsonインポート">
    </form>
    <?php
    include('./include/footer.php');  //フッター文を呼び出し
     ?>
  </body>
</html>

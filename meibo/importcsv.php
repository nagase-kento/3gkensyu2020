<!DOCTYPE html>
<?php
if (is_uploaded_file($_FILES["csvfile"]["tmp_name"])) {
    $file_tmp_name = $_FILES["csvfile"]["tmp_name"];
    $file_name = $_FILES["csvfile"]["name"];

    if (pathinfo($file_name, PATHINFO_EXTENSION) != 'csv') { //選択されてファイルがcsvか確認
        echo 'csvファイルのみ対応しています。<br>';
        include('./include/error.php');
    } else {
        //ファイルをdataディレクトリに移動
        if (move_uploaded_file($file_tmp_name, "/tmp/" . $file_name)) {
          //あとで削除でできるように権限を0644にする
            chmod("/tmp/" . $file_name, 0644);
            $msg = $file_name . "をアップロードしました。";
            $file = '/tmp/' .$file_name;
            $fp = fopen($file,"r");

            while (($data = fgetcsv($fp, 0, ",")) !== FALSE) { //配列に変換する
                $asins[] = $data;
            }
            fclose($fp); //ファイルの削除
            unlink('/tmp/'.$file_name);


            include("./include/functions.php");
            $pdo = Initdb();

            $array_fields = $asins[0]; //フィールド名の配列を作成
            $query_str_base = "INSERT INTO member(";
            foreach($array_fields as $each){
                $query_str_base .= $each . ",";
            }
            $query_str_base = substr($query_str_base, 0, -1); //最後の1文字削除
            $query_str_base .= ") VALUES (";

            array_splice($asins, 0, 1); //データ配列のフィールド名行を削除する
            echo "<hr>削除後の配列";
            echo "</hr><br/>";
            foreach ($asins as $each) {
                $query_str = $query_str_base;
                foreach ($each as $data) {
                    $query_str .= "'" . mb_convert_encoding($data, 'UTF-8', 'sjis-win') . "',";
                }
                $query_str = substr($query_str, 0, -1); //最後の1文字削除
                $query_str .= ")";
                echo $query_str . "<hr/>";

                try {
                    $sql = $pdo->prepare($query_str);
                    $sql->execute();

                    header('location:index.php'); //トップ画面にリダイレクトする
                    exit;

                } catch (PDOException $e) {
                    print $e->getMessage();
                }
            }
        } else {
            echo "ファイルをアップロードできません。<br>";
            include('./include/error.php');
        }
    }
}else {
    echo "ファイルが選択されていません。<br>";
    include('./include/error.php');
}
 ?>

<!DOCTYPE html>
<?php
include('./include/functions.php');
include('./include/statics.php');
$pdo = Initdb();

//member_idの値チェック
if (isset($_GET['member_ID']) && $_GET['member_ID'] != "") {
    $query_str = "SELECT m.member_ID,m.name,m.pref,m.seibetu,m.age,m.section_ID,sm.section_name,m.grade_ID,gm.grade_name
                  FROM member AS m
                  LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                  LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                  WHERE m.member_ID = " . $_GET['member_ID'] . "";
                  //DBのデータをmember_IDを指定して呼び出して

    $sql = $pdo->prepare($query_str);
    $sql ->execute();
    $result = $sql ->fetchAll();

 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="include/functions.js">
    </script>
    <title>社員名簿</title>
  </head>
  <body>
    <form name="mainform" action="entry_update02.php" method="POST">
    <?php
    include('./include/header.php');
     ?>
    <?php
    if (count($result) != 1) {
        echo "存在しない社員IDです<br/>";
        include('./include/error.php');
    }else {?>
    <div class="search_resurt">
      <table class='table table-bordered'>
        <tr>
          <th class="table-active"> 社員ID </th>
          <!-- 呼び出したDBのmember_IDを以下で表示 -->
          <td><?php echo $result[0]['member_ID']; ?></td>
        </tr>
        <tr>
          <th class="table-active"> 名前 </th>
          <td><input type="text" maxlength=30 name="namae" value="<?php echo $result[0]['name']; ?>"></td>
        </tr>
        <tr>
          <th class="table-active"> 出身地 </th>
          <!-- 都道府県のデータは、[./include/statics.php]に配列で部品化されているので、以下で呼び出して表示する。配列なのでforeachでループして表示する。 -->
          <td><select name="todouhuken">
            <option value="">都道府県</option>
            <?php
            foreach ($pref_array as $key => $value) {
                if ($key == $result[0]['pref']) { //if文で現在設定されている都道府県の時のみデフォルト値が付くようにする
                    echo "<option value='" . $key . "' selected>" . $value . "</option>";
                }else {
                    echo "<option value='" . $key . "'>" . $value . "</option>";
                }
            }
             ?>
              </select>
          </td>
        </tr>
        <tr>
          <th class="table-active"> 性別 </th>
          <!-- 性別のデータも、[./include/statics.php]に配列で部品化されているので、以下で呼び出して表示する。配列なのでforeachでループして表示する。 -->
          <td>
            <?php
            foreach ($gender_array as $key => $value) {
                if ($key == $result[0]['seibetu']) { //if文で現在設定されている性別の時のみデフォルト値が付くようにする
                    echo "<input type='radio' id='" . $key . "' name='seibetu' value='" . $key . "' checked><label for='" . $key . "'>" . $value . "　</label>";
                }else {
                    echo "<input type='radio' id='" . $key . "' name='seibetu' value='" . $key . "'><label for='" . $key . "'>" . $value . "　</label>";
                }
            }
             ?>
          </td>
        </tr>
        <tr>
          <th class="table-active"> 年齢 </th>
          <td><input type="number" name="age" value="<?php echo $result[0]['age']; ?>">才</td>
        </tr>
        <tr>
          <th class="table-active"> 所属部署 </th>
          <!-- 所属部署はDBにデータがあるので、DBのデータを[./include/functions.php]のfunction sections_arrayで配列にしてから、呼び出している -->
          <td>
            <?php
            //関数 sections_array()でDBの役職データを配列にする。
            $sections = sections_array();
            foreach ($sections as $each) {
                if ($each[0] == $result[0]['section_ID']) {
                    echo "<input type='radio' id='section" . $each[0] . "' name='busyo' value='" . $each[0] . "' checked><label for='section" . $each[0] . "'>" . $each[1] . "　</label>";
                }else {
                    echo "<input type='radio' id='section" . $each[0] . "' name='busyo' value='" . $each[0] . "'><label for='section" . $each[0] . "'>" . $each[1] . "　</label>";
                }
            }
            ?>
          </td>
        </tr>
        <tr>
          <th class="table-active"> 役職 </th>
          <!-- 役職もDBにデータがあるので、DBのデータを[./include/functions.php]のfunction grade_arrayで配列にしてから、呼び出している -->
          <td>
            <?php
            //関数 grade_array()でDBの部署データを配列にする。
            $grade = grade_array();
            foreach ($grade as $each) {
                if ($each[0] == $result[0]['grade_ID']) {
                    echo "<input type='radio' id='grade" . $each[0] . "' name='yakusyoku' value='" . $each[0] . "' checked><label for='grade" . $each[0] . "'>" . $each[1] . "　</label>";
                }else {
                    echo "<input type='radio' id='grade" . $each[0] . "' name='yakusyoku' value='" . $each[0] . "'><label for='grade" . $each[0] . "'>" . $each[1] . "　</label>";
                }
            }
             ?>
          </td>
        </tr>
      </table>
      <table align=right>
        <tr>
          <input type="hidden" name="member_ID" value="<?php echo $result[0]['member_ID']; ?>">
          <td><input type="button" value="登録" onClick="submitCheck();"></td>
          <td><input type="reset" value="リセット"></td>
        </tr>
      </table>
      <?php } ?>
      <?php
      // echo "<pre>";
      // var_dump($result);
      // echo "</pre>";
       ?>
    </div>
    <?php
}else {
    echo "社員IDが見つかりません<br/>";
    include('./include/error.php');
}
    ?>
    </form>
    <?php
    include('./include/footer.php');  //フッター文を呼び出し
     ?>
  </body>
</html>

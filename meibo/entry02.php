<!DOCTYPE html>
<?php
include('./include/functions.php');
include('./include/statics.php');
$pdo = Initdb();

//$checkparaでパラメータのチェック
$checkpara = array($_POST['namae'],$_POST['todouhuken'],$_POST['seibetu'],$_POST['age'],$_POST['busyo'],$_POST['yakusyoku'],$_POST['member_ID']);
if (checkpara($checkpara)) {
    $query_str = "INSERT INTO
                  member(name, pref, seibetu, age, section_ID, grade_ID)
                  VALUES ('" . $_POST['namae'] . "'," . $_POST['todouhuken'] . "," . $_POST['seibetu'] . "," . $_POST['age'] . "," . $_POST['busyo'] . "," . $_POST['yakusyoku'] . ")";
                  //SQL文のINSERTを実行し、新しいデータ（社員）を作成する

    $sql = $pdo->prepare($query_str);
    $sql ->execute();
    $id = $pdo ->lastInsertId("member_ID"); //新しく作成したデータ（社員）のmember_IDを取得

    //member_idの値チェックを行い、問題なければ新しく作成したデータ（社員）の詳細画面にリダイレクト
    if ($id != "") {
        $url = './detail01.php?member_ID=' . $id ;
        header('location: ' . $url );
        exit;
    }else {
        echo "社員IDが見つかりません<br/>";
        include('./include/error.php');
    }
}else {
    echo "パラメータが不正です。<br/>";
    include('./include/error.php');
}
 ?>

<!DOCTYPE html>
<?php
include('./include/statics.php');
include('./include/functions.php');
 ?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="include/functions.js">
    </script>
    <title>社員名簿</title>
  </head>
  <body>
    <?php
    include('./include/header.php');
     ?>
    <form name="mainform" action="entry02.php" method="POST">
      <!--ここからフォーム -->
      <div class="search_resurt">
      <table class='table table-bordered'>
        <tr>
          <th class="table-active">名前</th>
          <td><input type="text" maxlength="30" name="namae"></td>
        </tr>
        <tr>
          <th class="table-active">出身地</th>
          <td><select name="todouhuken">
            <option value="" selected>都道府県</option>
            <?php
            //部品化した都道府県の配列をループで回してプルダウンを作成
            foreach ($pref_array as $key => $value) {
                echo "<option value='" . $key . "'>" . $value . "</option>";
            }
             ?>
            </select>
          </td>
        </tr>
        <tr>
          <th class="table-active">性別</th>
          <td>
            <?php
            foreach ($gender_array as $key => $value) {
                if ($key == 1) {
                    echo "<input type='radio' id='seibetu" . $key . "' name='seibetu' value='" . $key . "' checked><label for='seibetu" . $key . "'>" . $value . "　</label>";
                }else {
                    echo "<input type='radio' id='seibetu" . $key . "' name='seibetu' value='" . $key . "'><label for='seibetu" . $key . "'>" . $value . "　</label>";
                }
           }
          ?>
         </td>
        </tr>
        <tr>
          <th class="table-active">年齢</th>
          <td><input type="number" name="age">才</td>
        </tr>
        <tr>
          <th class="table-active">所属部署</th>
          <td>
            <?php
            //関数 grade_array()でDBの部署データを配列にする。
            $sections = sections_array();
            foreach ($sections as $each) {
                if ($each[0] == 1) {
                    echo "<input type='radio' id='section" . $each[0] . "' name='busyo' value='" . $each[0] . "' checked><label for='section" . $each[0] . "'>" . $each[1] . "　</label>";
                }else {
                    echo "<input type='radio' id='section" . $each[0] . "' name='busyo' value='" . $each[0] . "'><label for='section" . $each[0] . "'>" . $each[1] . "　</label>";
                }
           }
            ?>
          </td>
        </tr>
        <tr>
          <th class="table-active">役職</th>
          <td>
            <?php
            //関数 sections_array()でDBの役職データを配列にする
            $grade = grade_array();
            foreach ($grade as $each) {
                if ($each[0] == 1) {
                    echo "<input type='radio' id='grade" . $each[0] . "' name='yakusyoku' value='" . $each[0] . "' checked><label for='grade" . $each[0] . "'>" . $each[1] . "　</label>";
                }else {
                    echo "<input type='radio' id='grade" . $each[0] . "' name='yakusyoku' value='" . $each[0] . "'><label for='grade" . $each[0] . "'>" . $each[1] . "　</label>";
                }
           }
            ?>
          </td>
        </tr>
      </table>
      <!-- ここまでフォーム -->
      <table align="right">
        <tr>
          <td><input type="button" value="登録" onclick="submitCheck();"></td>
          <td><input type="reset" value="リセット"></td>
        </tr>
      </table>
      </div>
    </form>
    <?php
    // echo "<pre>";
    // var_dump($grade);
    // echo "</pre>";
     ?>
    <?php
    include('./include/footer.php');  //フッター文を呼び出し
     ?>
  </body>
</html>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>連想配列練習画面02</title>
  </head>
  <body>
    <?php
    $me_data = array(
      'town' => '小平',
      'sport' => '野球',
      'hobby' => '映画',
      'age' => 27,
      'drink' => '水'
      );
      foreach ($me_data as $each) {
        echo $each . "<br/>";
      }
      foreach ($me_data as $key => $value) {
        echo $key . " : " . $value . "<br/>";
      }
      ?>
  </body>
</html>

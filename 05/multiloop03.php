<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習画面03</title>
  </head>
  <body>
    <?php
    $kaimono01 = array(
      'a' => 'トマト',
      'b' => '100円',
      'c' => '野菜',
      'd' => 3,
      'e' => '300円'
    );
    $kaimono02 = array(
      'a' => 'ジャガイモ',
      'b' => '50円',
      'c' => '野菜',
      'd' => 7,
      'e' => '350円'
    );
    $kaimono03 = array(
      'a' => 'キュウリ',
      'b' => '150円',
      'c' => '野菜',
      'd'=> 5,
      'e' => '750円'
    );
    $kaimono04 = array(
      'a' => '豚肉',
      'b' => '300円',
      'c' => '野菜',
      'd' => 3,
      'e' => '900円'
    );
    $kaimono05 = array(
      'a' => 'ソース',
      'b' => '200円',
      'c' => '野菜',
      'd' => 1,
      'e' => '200円'
    );
    $kaimono_all= array($kaimono01,$kaimono02,$kaimono03,$kaimono04,$kaimono05);
    echo "<table border='1' width='500' >";
    echo "<tr><th>品名</th>";
    echo "<th>単価</th>";
    echo "<th>ジャンル</th>";
    echo "<th>個数</th>";
    echo "<th>合計</th></tr>";
    foreach ($kaimono_all as $each) {
      echo "<tr><td>" . $each['a'] . "</td>";
      echo "<td>" . $each['b'] . "</td>";
      echo "<td>" . $each['c'] . "</td>";
      echo "<td>" . $each['d'] . "</td>";
      echo "<td>" . $each['e'] . "</td></tr>";
      }
    echo "</table>";
    ?>
  </body>
</html>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>連想配列練習画面01</title>
  </head>
  <body>
    <?php
    $me_data = array(
      'town' => '小平',
      'sport' => '野球',
      'hobby' => '映画',
      'age' => 27,
      'drink' => '水'
      );

      echo $me_data["age"];
      echo $me_data["hobby"];
      echo $me_data["fruit"];
      $me_data["from"] = "アメリカ";
      var_dump($me_data);
      ?>
      <pre>
        <?php var_dump($me_data); ?>
      </pre>
  </body>
</html>

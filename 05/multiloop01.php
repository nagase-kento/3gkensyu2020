<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>配列練習画面01</title>
  </head>
  <body>
    <table border="1">
    <?php
    $box_a = array("あ","い","う","え","お");
    $box_b = array("か","き","く","け","こ");
    $box_c = array("さ","し","す","せ","そ");
    $box_d = array("た","ち","つ","て","と");
    $box_e = array("な","に","ぬ","ね","の");

    $box_all = array($box_a,$box_b,$box_c,$box_d,$box_e);
    echo "<pre>";
    var_dump($box_all);
    echo "</pre>";

    echo "<hr>";
      for ($i=0; $i < count($box_all) ; $i++) {
        echo "<tr>";
        foreach ($box_all[$i] as $each) {
          echo "<td>" . $each . "</td>";
          }
        echo "</tr>";
        }
        ?>
    </table>
    <table border="1">
    <?php
    echo "<hr>";
      foreach ($box_all as $each) {
        echo "<tr>";
        foreach ($each as $i) {
          echo "<td>" . $i . "</td>";
          }
        echo "</tr>";
        }
    ?>
    </table>
  </body>
</html>
